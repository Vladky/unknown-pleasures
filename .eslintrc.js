module.exports = {
	env: {
		browser: true,
		es6: true
	},
	parserOptions: {
		sourceType: 'module'
	},
	rules: {
		quotes: ['error', 'single'],
		semi: ['error', 'never']
	}
}
