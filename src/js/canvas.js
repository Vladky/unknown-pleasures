document.body.innerHTML = '<canvas></canvas>'
const canvas = document.querySelector('canvas')
const c = canvas.getContext('2d')

canvas.width = innerWidth
canvas.height = innerHeight

addEventListener('resize', () => {
	canvas.width = innerWidth
	canvas.height = innerHeight
})

export default c
