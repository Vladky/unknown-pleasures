export const array = length => new Array(length).fill().map((x, i) => i)
const progression = (a, b, step = 1) =>
	new Array(Math.abs(a - b) / step).fill().map((x, i) => i * step + a)

const f = func => (range = [0, 10], offset = { x: 0, y: 0 }) =>
	progression(range[0], range[1]).map(x => ({
		x: x + (offset.x || 0),
		y: func(x) + (offset.y || 0)
	}))
export const r = (min, max) => Math.random() * (max - min) + min

export const randNormal = (mu, sigma) => {
	var sum = 0
	for (var i = 0; i < 6; i += 1) {
		sum += r(-1, 1)
	}
	var norm = sum / 6
	return mu + sigma * norm
}
// mu + (sigma * new Array(6).fill().reduce(a => a + r(-1, 1), 0)) / 6

export const normalPDF = (x, mu, sigma) => {
	var sigma2 = Math.pow(sigma, 2)
	var numerator = Math.exp(-Math.pow(x - mu, 2) / (2 * sigma2))
	var denominator = Math.sqrt(2 * Math.PI * sigma2)
	return numerator / denominator
}

export const dribble = x => x + Math.random() * 0.2
export const sin = x => Math.sin(x)
export default f
