import { Rect, Graph, Line } from './primitives.js'
import f, { dribble, randNormal, normalPDF, array, r } from './functions'
;(() => {
	const backgroundColor = 'black'
	const linesColor = 'white'
	const addBottomBlack = array =>
		array.reduce((a, r) => {
			a.push({
				x: r.x,
				y: r.y,
				callback: () => {
					new Line(r.x, r.y, r.x, r.y + 50, backgroundColor).draw()
				}
			})
			return a
		}, [])
	const objects = []
	const setup = () => {
		const w = innerWidth,
			h = innerHeight
		objects.push(new Rect(0, 0, w, h, backgroundColor))
		const lines = (count, paddingX, paddingY) => {
			const step = (h - paddingY * 2) / count
			return array(count).map(i => {
				const iterate = value => value + i * step
				const scale = value => (-40 + r(0, 1)) * value
				const nModes = ((x, a) => r(x - a, x + a))(100, 30)
				const mus = array(parseInt(nModes)).map(() => randNormal(w / 2, 200))
				const sigmas = array(parseInt(nModes)).map(() => randNormal(10, 10))
				const graphData = addBottomBlack(
					f(x => {
						const noise = array(parseInt(nModes)).reduce(
							(acc, i) => (acc += normalPDF(x, mus[i], sigmas[i])),
							0
						)
						return iterate(scale(noise))
					})([paddingX, w - paddingX], { y: paddingY })
				)
				return new Graph(graphData, linesColor)
			})
		}
		objects.push(...lines(80, (w - 400) / 2, (h - 400) / 2))
	}
	const draw = () => {
		objects.forEach(e => e.draw())
		var canvas = document.querySelector('canvas')
		var img = canvas.toDataURL('image/png')
		canvas.style = 'opacity: 0; position: absolute'
		document.write('<img src="' + img + '"/>')
	}
	setup()
	draw()
	addEventListener('resize', () => {
		draw()
	})
})()
