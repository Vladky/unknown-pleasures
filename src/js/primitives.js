import c from './canvas'

class Primitive {
	constructor() {
		this.c = c
	}
	draw() {}
}

export class Rect extends Primitive {
	constructor(x, y, width, height, color) {
		super()
		Object.assign(this, { x, y, width, height, color })
		this.c.fillStyle = this.color
		this.c.strokeStyle = this.color
	}
	fill() {
		this.c.fillRect(this.x, this.y, this.width, this.height)
	}
	stroke() {
		this.c.strokeRect(this.x, this.y, this.width, this.height)
	}
	draw() {
		this.fill()
	}
}

export class Graph extends Primitive {
	constructor(data, color = 'white') {
		super()
		Object.assign(this, { data, color })
	}
	draw() {
		this.data.forEach((e, i) => {
			this.c.lineWidth = 2.0
			this.c.beginPath()
			this.c.strokeStyle = this.color
			this.c.moveTo(
				this.data[i > 0 ? i - 1 : 0].x,
				this.data[i > 0 ? i - 1 : 0].y
			)
			this.c.lineTo(e.x, e.y)
			this.c.stroke()
			!!e.callback ? e.callback() : () => {}
		})
	}
}

export class Line extends Primitive {
	constructor(x, y, dx, dy, color = 'black') {
		super()
		Object.assign(this, { x, y, dx, dy, color })
		this.c.lineWidth = 1.6
	}
	draw() {
		this.c.beginPath()
		this.c.strokeStyle = this.color
		this.c.moveTo(this.x, this.y)
		this.c.lineTo(this.dx, this.dy)
		this.c.stroke()
	}
}
