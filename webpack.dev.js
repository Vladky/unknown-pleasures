/* eslint-disable no-undef */
const path = require('path')
const merge = require('webpack-merge')
const common = require('./webpack.common')

module.exports = merge(common, {
	mode: 'development',
	devServer: {
		contentBase: path.resolve(__dirname, 'dist'),
		hot: true,
		writeToDisk: true,
		port: 8082
	},
	watch: true,
	devtool: 'source-map'
})